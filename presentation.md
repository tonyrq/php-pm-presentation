# Tony Quilkey

- Senior Architect / Engineer at Glow
- PHP dev since early 2000s
- Passionate about open source, community and brewing beer.

_Twitter:_ __@tonyrq__
_Github:_ __@trq__
_Glow:_ __https://glowfeed.com__

---

# PPM - PHP Process Manager
- Process manager, supercharger and load balancer for PHP applications
- Built on top of ReactPHP [^1]
- Been around since Feb 2014
- Started getting some notice more recently (4797 Github stars)
- Not production ready [^2]

[^1]: Event-driven, non-blocking I/O with PHP

[^2]: https://github.com/php-pm/php-pm#issues

---

# Features
- Performance boost up to 15x
- Integrated load balancer
- Support for HttpKernel (Symfony/Laravel), Drupal (experimental), Zend (experimental)

---

# How does it work?

- A single PPM server (worker manager) listening on a particular port
- Server can be configued to run any number of workers
- Each worker is a bootstrapped instance of your app

---

# What might a deployment look like?


## A few different possible configurations
- Access PPM server directly
- Nginx ProxyPass directly to workers

^ Now serves static files

---

![inline](workers.jpg)

---

# PPM Workers Explained

But first...

---

# Bootstrapping a Symfony app
```php
<?php

use Symfony\Component\HttpFoundation\Request;

require __DIR__.'/../vendor/autoload.php';

$kernel = new AppKernel('prod', true);
$request = Request::createFromGlobals();

$response = $kernel->handle($request);

$response->send();
$kernel->terminate($request, $response);
```

---

```php
/**
 * Kernel
 */
public function handle(Request $request, $type = HttpKernelInterface::MASTER_REQUEST, $catch = true)
{
    if (false === $this->booted) {
        $this->boot();
    }
    return $this->getHttpKernel()->handle($request, $type, $catch);
}
```

---

```php

/**
 * Kernel
 */
public function boot()
{
    if (true === $this->booted) {
        return;
    }

    if ($this->loadClassCache) {
        $this->doLoadClassCache($this->loadClassCache[0], $this->loadClassCache[1]);
    }

    // init bundles
    $this->initializeBundles();

    // init container
    $this->initializeContainer();

    foreach ($this->getBundles() as $bundle) {
        $bundle->setContainer($this->container);
        $bundle->boot();
    }

    $this->booted = true;
}
```
---

# Bootstrapping PPM Workers (and your app)

- Concept of _Bootstrapers_ (BootstrapInterface)

^ Used to bootstrap app
^ Bootstraps exist for Symfony, Laravel, Drupal and Zf2

- Concept of _Bridges_ (BridgeInterface)

^ Used to bridge React Request/Response to app Request/Response
^ Bridges exists for HttpKernel, Drupal and Zend
^ Laravel uses HttpKernel same as Symfony

- Easy enough to create new _Bootstraps_ and _Bridges_

---

```php
/**
 * Bootstraps\Symfony
 */
public function getApplication()
{
    // ...
    
    $class = class_exists('\AppKernel') ? '\AppKernel' : '\App\Kernel';
        
    $app = new $class($this->appenv, $this->debug);

    // Wrapper for \Closure::bind()
    Utils::bindAndCall(function() use ($app) {
        $app->initializeBundles();
        $app->initializeContainer();
    }, $app);
    
    Utils::bindAndCall(function() use ($app) {
        foreach ($app->getBundles() as $bundle) {
            $bundle->setContainer($app->container);
            $bundle->boot();
        }
        $app->booted = true;
    }, $app);

    return $app;
}
```

---

```php
/**
 * Bridges\HttpKernel
 */
public function bootstrap($appBootstrap, $appenv, $debug)
{
    // ...

    if ($this->bootstrap instanceof BootstrapInterface) {
        $this->application = $this->bootstrap->getApplication();
    }

    // ...
}
```

---

# How do PPM workers handle a request?

```php
/**
 * Bridges\HttpKernel
 */
public function handle(ServerRequestInterface $request)
{
    // ...

    $syRequest = $this->mapRequest($request);
   
    // ...

    $syResponse = $this->application->handle($syRequest);

    // ...
}
```

---
# Conclusion

- Production readyness?

---

# Demo

- Symfony 3 demo app
- Added sqlite PHP extension to the PPM docker image

---

# Thanks

- Twitter: @tonyrq
- Github: @trq
- Glow: https://glowfeed.com

---